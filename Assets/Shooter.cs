﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour {
	public GameObject bullet;
	public float speedFactor; 
	public float delay;
	public Transform weak;

	// Use this for initialization
	void Start () {
		StartCoroutine (Shoots ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	IEnumerator Shoots (){
		while (true) {
			yield return new WaitForSeconds (delay);
			GameObject clone = Instantiate (bullet, new Vector2 (transform.position.x +2f , transform.position.y),Quaternion.identity) as GameObject; 
			clone.GetComponent<Rigidbody2D> ().velocity = transform.right * speedFactor;
		}
	}
	void OnCollisionEnter2D ( Collision2D col) {
		if (col.gameObject.tag == "Player") {
			float height = col.contacts [0].point.y - weak.position.y;
			if (height > 0) {
				Destroy (this.gameObject);
				col.gameObject.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
				col.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, 500));
			} else {
				col.gameObject.GetComponent<Character> ().TakeDamage (50);
				col.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (250, 500));
			}
		}
	}
}
