﻿using UnityEngine;
using System.Collections;

public class checkpoint : MonoBehaviour {
	public enum state { Inactive , Active};
	public state status;
	public Sprite[] sprites;
	public checkpointHandler ch;
    

	// Use this for initialization
	void Start () {
		ch = GameObject.Find ("chekpointHandler").GetComponent <checkpointHandler> ();
	
	}
	
	// Update is called once per frame
	void Update () {
		ChangeStatus ();
	
	}
	void ChangeStatus (){
		if (status == state.Inactive) {
			GetComponent<SpriteRenderer> ().sprite = sprites [0];
		}
		if (status == state.Active) {
			GetComponent <SpriteRenderer> ().sprite = sprites [1];
            
        }
	}
	void OnTriggerEnter2D ( Collider2D other ){
		if (other.gameObject.tag == "Player") {
			ch.UpdateCheckpoint (this.gameObject);
           
		}
	}
}
