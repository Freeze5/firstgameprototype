﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Lerp_script : MonoBehaviour {
	public float time;
	public bool isRight;
	public GameObject zombie;
	public Vector2 StartPos;
	public Vector2 EndPos;
	public Vector2  RightPosCol;
	public Vector2 LeftPosCol;
	public bool isAttack;
	public int Health;
	public Image HealthZombieBar;
	public bool die;
	public float timer;
	public bool dontMove;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!die) {
			if (Mathf.Round (transform.position.x) == (int)StartPos.x) { //если позиция по иксу приближается к 
				isRight = false;// переменная бул фалс
			}
			if (Mathf.Round (transform.position.x) == (int)EndPos.x) { // если позиция по иксу приближается к 
				isRight = true;// переменная бул тру
			}
			Move ();
		}


		if (Health <= 0) {
			Health = 0;
		}
	}
	void Move (){


		if (isRight == true) //  если бул тру тогда двигай обьект вправо на (0,0)
		{

			transform.position = Vector2.MoveTowards (transform.position, StartPos, time);
			GetComponent<SpriteRenderer> ().flipX = false;
			ChangePosCol (RightPosCol);

		}


		else if (isRight == false)// если бул фолс тогда двигай обьект влево на (0,0)
		{

			transform.position = Vector2.MoveTowards (transform.position, EndPos, time);
			GetComponent<SpriteRenderer> ().flipX = true;
			ChangePosCol (LeftPosCol);



		}
		
	}
	void ChangePosCol( Vector2 Pos){
		BoxCollider2D[] mas = GetComponents < BoxCollider2D> ();
		for (int i = 0; i < mas.Length; i++) {
			if (mas [i].isTrigger == true) {
				mas [i].offset = Pos;
			}
		}
	}
	void OnTriggerStay2D(Collider2D col){
		if (col.gameObject.tag == "player") {
			dontMove = true;
			timer = timer + Time.deltaTime;
			if(timer >= 1.5f){
			GetComponent<Animator> ().SetBool ("isAttack", true);
			col.gameObject.GetComponent<Character> ().TakeDamage (20);
				timer = 0;
			}
		}
	}
	 void OnTriggerExit2D ( Collider2D col){
		if (col.gameObject.tag == "player") {
			GetComponent<Animator> ().SetBool ("isAttack", false);
		}
	}
	public void TakeZDamage ( int CountDamage){
		Dead (Health);
		Health -= CountDamage;
		HealthZombieBar.fillAmount -= (float)CountDamage / 50;
	}
	public void Dead ( int Over){
		if (Over == 0) {
			die = true;
			GetComponent<Animator> ().SetBool ("die", true);
		}
	}

}
