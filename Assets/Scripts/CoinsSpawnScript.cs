﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsSpawnScript : MonoBehaviour {

    public Transform[] spawnPoints;
    public GameObject coinPrefab;
    public GameObject[] coinsMassive;
    public GameObject platform;
 

	// Use this for initialization
	void Start () {
        if (GameObject.Find("coin(Clone)"))
        {
            Debug.Log("This Object (coins) already exist");
         }
        else {
           Invoke( "CreateCoins",2f);
        }
	}
	
	void CreateCoins()
    {
        for(int i=0;i < spawnPoints.Length; i++)//на каждую точку спавна
        { 
            GameObject temporary = 
                Instantiate(coinPrefab, new Vector2(spawnPoints[i].transform.position.x, spawnPoints[i].transform.position.y), transform.rotation) as GameObject;
            coinsMassive[i] = temporary;
        }
        coinsMassive[10].transform.parent = platform.transform;
        coinsMassive[11].transform.parent = platform.transform;
        coinsMassive[12].transform.parent = platform.transform;
    }

    
}
