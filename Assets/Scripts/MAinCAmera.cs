﻿using UnityEngine;
using System.Collections;

public class MAinCAmera : MonoBehaviour {
	public Transform hero;
	public float speed;
	public GameObject character;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (character.transform.localScale.x == 0.6f) {
			transform.position = Vector3.Lerp (transform.position, new Vector3 (hero.position.x + 4f, hero.position.y, -10), speed );
		} else {
			transform.position = Vector3.Lerp (transform.position, new Vector3 (hero.position.x - 4f, hero.position.y, -10), speed );
		}
	
	}
}
