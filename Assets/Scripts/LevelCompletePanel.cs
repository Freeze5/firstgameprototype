﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCompletePanel : MonoBehaviour {
    public GameObject lvlCompletedPanel;

    public bool panelON;
    public float maxTimer = 1f;
    public float timer;

    public int BossHPChecker;
    

    void Start () {
        panelON = false;
	}
	
	
	void Update () {
        BossHPChecker = PlayerPrefs.GetInt("BossHP");
        
        if (panelON)
            timer += Time.deltaTime;
        else
            timer = 0;
        ActivatePanel();
        FreezeTime();



    }


    void FreezeTime()
    {
        if (timer > maxTimer)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;
    }

    void ActivatePanel()
    {
        if (BossHPChecker <= 0)
        {
            panelON = true;
            lvlCompletedPanel.GetComponent<Animator>().SetBool("lvlCompleted", true);
            
        }
    }
}
