﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PatrollBoss : MonoBehaviour 
{
    //move variables
    public Transform[] patrolpoint;
    int currentPoint;
    public float speed = 0.2f;
    Animator anim;
    public float sight =20f;
    //health variables
    public int Health;
    public Image HPBar;
    //bool variables
    public bool isDead;
    public bool isAttack;
    public bool isWalk;
    // checking hero distance variables
    public GameObject hero;
    public float distance;
    // fighting hero variables
    public float Timer;
    public bool iStay;
    Rigidbody2D rigidbody;
    public int heroHPChecker;

    private AudioSource audioSource;
    public AudioClip damaged;

    public Transform damagePoint;
   
   
    

   
    
    // Use this for initialization
    void Start()
    {
        
        anim = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody2D>();
        anim.SetBool("isWalk", true);
        iStay = false;
        hero = GameObject.Find("character");
        Health = PlayerPrefs.GetInt("BossHP");
        HPBar.fillAmount = 1f;

        
    }

    // Update is called once per frame
    void Update()
    {
        if (Mathf.Round(transform.position.x) == (int)patrolpoint[0].position.x)
        {
            isWalk = false;
        }
        if (Mathf.Round(transform.position.x) == (int)patrolpoint[1].position.x)
        {
            isWalk = true;
        }
        distance = Vector3.Distance(transform.position, hero.transform.position);

        MoveHorizontal();
        patrolBossBehavior();

        heroHPChecker = PlayerPrefs.GetInt("Health");
        BossVictory();
        
        Dead(Health);

   
    }
    void MoveHorizontal()
    {
        if (!isDead)
        {
            if (!iStay)
            {
                if (isWalk)
                {
                    transform.position = Vector2.MoveTowards(transform.position, patrolpoint[0].position, speed);
                    GetComponent<SpriteRenderer>().flipX = false;
                    
                }
                else if (!isWalk)
                {
                    transform.position = Vector2.MoveTowards(transform.position, patrolpoint[1].position, speed);
                    GetComponent<SpriteRenderer>().flipX = true;
                    
                }
            }
        }
    }

   
    public void patrolBossBehavior()
    {
        //если дистанция меньше поля зрениЯ и больше 3-х
        if (distance < sight && distance>3f)
        {
           
            iStay = false;
            isAttack = false;
            anim.SetBool("isRage", true);//анимация бега вкл
            anim.SetBool("isWalk", false);//анимация хотьбы выкл
            anim.SetBool("isAttack", false);
            speed = 0.25f;
        }
        else if (distance > sight)//если дистанция больше поля зрения
        {
            iStay = false;//можно идти
            isAttack = false;
           
            anim.SetBool("isRage", false);//бег выкл
            anim.SetBool("isWalk", true);//патрулирование вкл
            anim.SetBool("isAttack", false);
            speed = 0.15f;
        }
        else if (distance < 3f)//если дистанция меньше 3-х
        {
          
            iStay = true;//нельзя идти -> остановится
            rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
            isAttack = true;//атака true
            Timer += Time.deltaTime;
            anim.SetBool("isRage", false);
            anim.SetBool("isAttack", true);
            if (isAttack && Timer >= 1.2f && heroHPChecker !=0)//если атка true то бить каждые 1.2 секунды
            {
                hero.GetComponent<Character>().TakeDamage(20);
                Timer = 0;// обнулить таймер
            }
            else if (!isAttack )
            {
                anim.SetBool("isAttack", false);
            }
        }
        else
        {
            isAttack = false;
            iStay = false;
            anim.SetBool("isWalk", true);
            anim.SetBool("isAttack", false);
            speed = 0.1f;
        }
        
    }

    void ChasingHero()
    {
        transform.position = Vector2.MoveTowards(transform.position, hero.transform.position, speed * Time.deltaTime);
    }

    //получить урон
    public void RestrictHealth(int Count)
    {
        Dead(Health);
        Health -= Count;
        HPBar.fillAmount -= (float)Count / 200;
        PlayerPrefs.SetInt("BossHP", Health);
        audioSource.PlayOneShot(damaged);
    }
    public void Dead(int Over)
    {
        if (Over <= 0)
        {
            Health = 0;
            isDead = true;
            anim.SetBool("isDead", true);
        }
    }
    public void BossVictory()
    {
        if (heroHPChecker <= 0)
        {
            iStay = true;//нельзя идти -> остановится
            isAttack = false;
            rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
            anim.SetBool("isVictory", true);
            anim.SetBool("isAttack", false);
        }
        else
        {
            anim.SetBool("isVictory", false);
            
        }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (!isDead)
        {
            if (col.gameObject.tag == "throw")
            {
                RestrictHealth(10);
                Destroy(col.gameObject);
                GetComponent<Animator>().SetTrigger("GetHit");
                iStay = true;
            }
        }
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            float height = col.contacts[0].point.y - damagePoint.position.y;
            if (height > 0)
            {
                col.gameObject.GetComponent<Character>().TakeDamage(20);
                col.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(250, 500));
            }
            else
            {
                return;
            }
        }
    }

}