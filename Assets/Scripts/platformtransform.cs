﻿using UnityEngine;
using System.Collections;

public class platformtransform : MonoBehaviour {
	public bool isVertical;
	public Vector2 EndPos;
	public Vector2 StartPos;
	public bool isUp;
	public bool isRight;
	public float speed;


	// Use this for initialization
	void Start () {
		StartPos = transform.position;
	
	}

	// Update is called once per frame

	void Update()
	{
		
		if (isVertical) {
			MoveVertical ();
			if (Mathf.Round (transform.position.y) ==(float) EndPos.y) {
				isUp = true;
			}
			if (Mathf.Round (transform.position.y) ==(float) StartPos.y) {
				isUp = false;
			}
		}

		if (!isVertical) {
			MoveHorizontal ();
			if (Mathf.Round (transform.position.x) == (float) EndPos.x) {
				isRight = true;
			}
			if (Mathf.Round (transform.position.x) ==(float) StartPos.x) {
				isRight = false;
			}
		}
		
	}
	void MoveHorizontal (){
		if (isRight) {
			transform.position = Vector2.Lerp (transform.position, StartPos, speed);
		} else {
			transform.position = Vector2.Lerp (transform.position, EndPos, speed);
		}
	}
	void MoveVertical (){
		if (isUp) {
			transform.position = Vector2.Lerp (transform.position, StartPos, speed);
		} else {
			transform.position = Vector2.Lerp (transform.position, EndPos, speed);
		}
	}
}
