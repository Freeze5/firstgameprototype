﻿using UnityEngine;
using System.Collections;

public class BaT : MonoBehaviour {
	public bool isHorizontal;
	public bool isRight;
	public Vector2 StartPos;
	public Vector2 EndPos;
	public float speed ;
	public Transform weakness;

	// Use this for initialization
	void Start () {
		StartPos = transform.position;
	
	}
	
	// Update is called once per frame
	void Update () {
		if ( isHorizontal){
			MoveHorizontal ();
			if (Mathf.Round (transform.position.x) == (float)StartPos.x) {
				isRight = true;
			} else {
				if (Mathf.Round (transform.position.x) == (float)EndPos.x) {
					isRight = false;
				}
			}
		}
	
	}
	void MoveHorizontal (){
		if (isRight) {
			transform.position = Vector2.MoveTowards (transform.position, EndPos, speed);
			GetComponent<SpriteRenderer> ().flipX = true;
		} else {
			transform.position = Vector2.MoveTowards (transform.position, StartPos, speed);
			GetComponent<SpriteRenderer> ().flipX = false;
		}
	}
	void OnCollisionEnter2D( Collision2D col){
		if (col.gameObject.tag == "Player") {
			float height = col.contacts [0].point.y - weakness.position.y;
			if (height > 0) {
				Destroy (gameObject);
				col.gameObject.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
				col.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, 250));

			} else {
				col.gameObject.GetComponent<Character> ().TakeDamage (20);
				col.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (100, 250));

			}

		}
	}

}
