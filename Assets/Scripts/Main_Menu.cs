﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Main_Menu : MonoBehaviour {

    public Button StartBtn, ContinueBtn;
    public bool isStart = false;
    public bool isContinue = false;

    private void Update()
    {
        ButtonActivity();
    }

    public void PlayGame (){
        PlayerPrefs.SetInt("MyCoins", 0);
        PlayerPrefs.SetInt("Health", 100);
        PlayerPrefs.SetFloat("HeroX", 273.31f);
        PlayerPrefs.SetFloat("HeroY", 110.3564f);
        PlayerPrefs.SetInt("BossHP", 200);
        BoolPlayerPrefs.SetBool("is2jump",false);
        BoolPlayerPrefs.SetBool("canFly", false);
        BoolPlayerPrefs.SetBool("canThrow", false);
        isStart = true;
        // SceneManager.LoadScene("Level1"); // didnt use this coz of <SceneLoading> script



    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Continue()
    {
        // SceneManager.LoadScene("Level1");
        BoolPlayerPrefs.GetBool("is2jump");
        BoolPlayerPrefs.GetBool("canFly");
        BoolPlayerPrefs.GetBool("canThrow");
        isContinue = true;
    }

    void ButtonActivity()
    {
        if (isStart)
            ContinueBtn.interactable = false;
        if (isContinue)
            StartBtn.interactable = false;
    }
}
