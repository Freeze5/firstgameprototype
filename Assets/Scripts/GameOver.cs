﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameOver : MonoBehaviour {
	public GameObject lvl;// уровень
	public GameObject panel;// панель GAmeOver
    public GameObject hero;
    public GameObject spawnPoint;
    public Button PauseBtn;


    void Start()
    {
        hero = GameObject.Find("character");
        spawnPoint = GameObject.Find("SpawnPoint");
        
    }

    //Application.LoadLevel ("Level1");
    public void Replay(){
		lvl.SetActive (true);
		panel.SetActive (false);
        PlayerPrefs.SetInt("Health", 100);
        PlayerPrefs.SetInt("MyCoins", 0);
        hero.transform.position = spawnPoint.transform.position;
        PauseBtn.GetComponent<Button>().interactable = true;
        BoolPlayerPrefs.SetBool("is2jump", false);
        BoolPlayerPrefs.SetBool("canFly", false);
		BoolPlayerPrefs.SetBool ("canThrow", false);
        SceneManager.LoadScene("Level1");
		return;
	}
    //Application .LoadLevel ("shop");
    public void Shop(){
		lvl.SetActive (false);
		panel.SetActive (false);
		
        SceneManager.LoadScene("shop");
	}
    public void LastCP()
    {
        PauseBtn.GetComponent<Button>().interactable = true;
        panel.SetActive(false);
        lvl.SetActive(true);
    }
}
