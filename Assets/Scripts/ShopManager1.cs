﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShopManager1 : MonoBehaviour {
    public Text currentCoins;
    
	// Use this for initialization
	void Start () {
        currentCoins.text = PlayerPrefs.GetInt("MyCoins").ToString();
	}
	
	public void BuyHealthPoints ( int cost)
    {
            if (PlayerPrefs.GetInt("MyCoins") >= cost)
            {
                PlayerPrefs.SetInt("MyCoins", PlayerPrefs.GetInt("MyCoins") - cost);
                currentCoins.text = PlayerPrefs.GetInt("MyCoins").ToString();
                PlayerPrefs.SetInt("Health", PlayerPrefs.GetInt("Health") + 20);
            }
    }
    public void MainMenuButton()
    {
        //SceneManager.LoadScene("Main Menu");// didnt use this coz of <SceneLoading> script
    }
    public void BackToGame()
    {
        //SceneManager.LoadScene("Level1"); didnt use this coz of <SceneLoading> script
        Time.timeScale = 1;
        PlayerPrefs.GetInt("Health");
        PlayerPrefs.GetInt("BossHP");
        BoolPlayerPrefs.GetBool("is2jump");
        BoolPlayerPrefs.GetBool("canFly");
        BoolPlayerPrefs.GetBool("canThrow");
        
    }

}
