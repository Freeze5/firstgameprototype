﻿using UnityEngine;
using System.Collections;

public class DeadZone_script : MonoBehaviour {
	public GameObject arrow;
	public GameObject DeadZone;
	public float move;
	public float speed ;


	// Use this for initialization
	void Start () {
		
	
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (move * speed, GetComponent<Rigidbody2D>().velocity.y);
	
	}
}
