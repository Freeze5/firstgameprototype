﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HeroSoundManager : MonoBehaviour {

    public static AudioClip heroBonus, heroDeath, heroJump, heroPoints , heroDeath2 , throwing , steps;
    static AudioSource audioSource;

	// Use this for initialization
	void Start () {
        heroBonus = Resources.Load<AudioClip>("heroBonus");
        heroDeath = Resources.Load<AudioClip>("heroDeath");
        heroJump = Resources.Load<AudioClip>("heroJump");
        heroPoints = Resources.Load<AudioClip>("heroPoints");
        heroDeath2 = Resources.Load<AudioClip>("dead");
        throwing = Resources.Load<AudioClip>("throwing");
        steps = Resources.Load<AudioClip>("footsteps");
        audioSource = GetComponent<AudioSource>();
    }

    public static void PlaySound ( string clip)
    {
        switch (clip)
        {
            case "Bonus":
                audioSource.PlayOneShot(heroBonus);
                break;
            case "Death":
                audioSource.PlayOneShot(heroDeath);
                break;
            case "Jump":
                audioSource.PlayOneShot(heroJump);
                break;
            case "Points":
                audioSource.PlayOneShot(heroPoints);
                break;
            case "death2":
                audioSource.PlayOneShot(heroDeath2);
                break;
            case "throw":
                audioSource.PlayOneShot(throwing);
                break;
            case "steps":
                audioSource.PlayOneShot(steps);
                break;
               
        }
           
    }

	
	
}
