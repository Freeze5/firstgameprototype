﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoading : MonoBehaviour {

    public int sceneID;

    public Image progressImg;
    public Text progressText;
	
	void Start () {
        StartCoroutine(AsyncLoad());
	}
	
	IEnumerator AsyncLoad()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneID);
        while (!operation.isDone)
        {
            float progress = operation.progress / 0.9f;
            progressImg.fillAmount = progress;
            progressText.text = string.Format("{0:0%}", progress);
            yield return null;

        }

    }
}
