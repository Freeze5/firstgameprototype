﻿using UnityEngine;
using System.Collections;

public class lift3 : MonoBehaviour {
	public Vector2 StartPos;
	public Vector2 EndPos;
	public bool isVertical;
	public bool isUp;
	public bool isRight;
	public float speed;

	// Use this for initialization
	void Start () {
		StartPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (isVertical) {
			MoveVertical ();
			if (Mathf.Round (transform.position.y) == (float)StartPos.y) {
				isUp = true;
			}
			if (Mathf.Round (transform.position.y) == (float)EndPos.y) {
				isUp = false;
			}
		}
		if (!isVertical) {
			MoveHorizontal ();
			if (Mathf.Round (transform.position.x) == (float)StartPos.x) {
				isRight = true;

			}
			if (Mathf.Round (transform.position.x) == (float)EndPos.x) {
				isRight = false;
			}
		}
	
	}
	void MoveVertical (){
		if (isUp) {
			transform.position = Vector2.MoveTowards (transform.position, EndPos, speed); 
		} else {
			transform.position = Vector2.MoveTowards (transform.position, StartPos, speed);
		}
	}
	void MoveHorizontal (){
		if (isRight) {
			transform.position = Vector2.MoveTowards (transform.position, EndPos, speed); 
		} else {
			transform.position = Vector2.MoveTowards (transform.position, StartPos, speed);
		}
	}
}
