﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Lerp_4_Image : MonoBehaviour {
	public Image image;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		ChangeA ();
	
	}
	void ChangeA (){
		image.color = Color.Lerp (image.color, new Color (image.color.r, image.color.g, image.color.b, 0f), 0.05f);
	}

}
