﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowingSmth : MonoBehaviour {
    public GameObject throwPref;
    public bool canThrow;
    
    public Vector2  offset = new Vector2 ( 0.5f, 0.1f);
    public float couldawn = 1.2f;
    public float lastTthrowDate;
    public Vector2 velocity;
    public GameObject trap;
    public GameObject throwingButton;

    




	void Start () {
        canThrow = false;
       
        lastTthrowDate = Time.time;
        throwingButton = GameObject.Find("ThrowButton");
        throwingButton.SetActive(false);
	}
	 void Update ()
    {
        canThrow = BoolPlayerPrefs.GetBool("canThrow");
        
        if (canThrow)
        {
            throwingButton.SetActive(true);
            trap.GetComponent<Animator>().SetBool("isTrap", true);
        }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "throw")
        {
            Destroy(col.gameObject);
            canThrow = true;
            BoolPlayerPrefs.SetBool("canThrow", true);
            HeroSoundManager.PlaySound("Bonus");
            trap.GetComponent<Animator>().SetBool("isTrap", true);
        }
    }
    void SetTransformX(float n)
    {
        transform.position = new Vector3(n, transform.position.y, transform.position.z);
    }

    public void ThrowObject()
    {
        if (canThrow && Time.time - lastTthrowDate > couldawn)
        {
            GameObject go = Instantiate(throwPref, (Vector2)transform.position + offset * transform.localScale.x, transform.rotation) as GameObject;
            if (transform.localScale.x == 1)
            {
                go.GetComponent<SpriteRenderer>().flipX = true;

            }
            else if (transform.localScale.x == -1)
            {
                go.GetComponent<SpriteRenderer>().flipX = false;
            }
            go.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity.x * transform.localScale.x, velocity.y);
            GetComponent<Animator>().SetTrigger("throw");
            HeroSoundManager.PlaySound("throw");
            lastTthrowDate = Time.time;

        }
    }
   
}
