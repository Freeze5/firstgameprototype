﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.UI;

public class GameControll : MonoBehaviour {

    public static GameControll control;

    public Character hero;


    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

        PlayerData playerData = new PlayerData();
        playerData.health = hero.Health;
       

        bf.Serialize(file, playerData);
        file.Close();
    }

    public void Load()
    {
        if(File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file =  File.Open(Application.persistentDataPath + "playerInfo.dat", FileMode.Open);
            PlayerData playerData = bf.Deserialize(file) as PlayerData;
            file.Close();

            hero.Health = playerData.health;
            
        }
    }
	
}

[Serializable]
class PlayerData
{
    public int health;
    public Image healthBar;
}
