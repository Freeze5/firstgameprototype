﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Werewolf : MonoBehaviour {
	public bool isWalk;
	public bool iGO;
	public bool isRun;
	public bool isAttack;
	public bool isDead;
	public int Health;
	public Image HEalthBar;
	public Vector2 StartPos;
	public Vector2 EndPos;
	public float speed;
	public float Timer;
	public Vector2 RightPos;
	public Vector2 LeftPos;

	// Use this for initialization
	void Start () {
		StartPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (Health <= 0) {
			Health = 0;
		}
		if ( Mathf.Round ( transform.position.x) == (int) StartPos.x){
			isWalk = false;
		}
		if (Mathf.Round (transform.position.x) == (int)EndPos.x) {
			isWalk = true;
		}
		MoveHorizontal ();
	}
	void MoveHorizontal (){
		if (!isDead) {
			if (!iGO) {
				if (isWalk) {
					transform.position = Vector2.MoveTowards (transform.position, StartPos, speed);
					GetComponent<SpriteRenderer> ().flipX = true;
					ChangePosCol (LeftPos);
				} else if (!isWalk) {
					transform.position = Vector2.MoveTowards (transform.position, EndPos, speed);
					GetComponent<SpriteRenderer> ().flipX = false;
					ChangePosCol (RightPos);
				}
			}
		
		}
	}
	public void GameOver ( int Over ){
		if (Over == 0) {
			Over = 0;
			GetComponent<Animator> ().SetBool ("isDead", true);
			isDead = true;
		}
	}
	void ChangePosCol ( Vector2 Pos ) {
		BoxCollider2D[] mas = GetComponents<BoxCollider2D> ();
		for (int i = 0; i < mas.Length; i++) {
			if (mas [i].isTrigger == true) {
				mas [i].offset = Pos;
			}
		}
	}
	 void OnTriggerStay2D (Collider2D other){
		if (other.gameObject.name == "character") {
			GetComponent<Animator> ().SetBool ("isAttack", true);
			iGO = true;
			Timer += Time.deltaTime;
			if (Timer >= 1.5f && isDead == false) {
				other.gameObject.GetComponent<Character> ().TakeDamage (10);
				Timer = 0;
			}

		}
	}
	void OnTriggerExit2D ( Collider2D other){
		if (other.gameObject.name == "character") {
			GetComponent<Animator> ().SetBool ("isAttack", false);
			iGO = false;
		}
	}
	public void TakeDamage ( int CountDamage){
		GameOver (Health);
		Health -= CountDamage;
		HEalthBar.fillAmount -= (float)CountDamage / 100;
	}

}
