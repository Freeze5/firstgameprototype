﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class checkpointHandler : MonoBehaviour {
	public GameObject[] CHECPOINTS;
	public GameObject player;
    public GameObject Boss;
    public Button yourButton;
    //PlayerPrefs
    public int healthSaver;
    public int BossHPSaver;
    public bool doublejumpChecker;
    public bool parashuteChecker;
    public bool canthrowChecker;
    public GameObject canthrowSpawn;
    public GameObject throwObject;
    public GameObject trapObject;





    // Use this for initialization
    void Start () {
		CHECPOINTS = GameObject.FindGameObjectsWithTag ("checkpoint");
        Button btn = yourButton.GetComponent<Button>();
        btn.interactable = false;
        btn.onClick.AddListener(TaskOnCkick);
       
	}
	
	
    public void TaskOnCkick()
    {
        foreach (GameObject cp in CHECPOINTS)
        {
            if (cp.GetComponent<checkpoint>().status == checkpoint.state.Active)
            {
                player.transform.position = new Vector3(cp.transform.position.x +2f, cp.transform.position.y, cp.transform.position.z -1f);

            }

        }
        player.GetComponent<Character>().isDead = false;
        player.GetComponent<Animator>().SetBool("isDead", false);
        player.GetComponent<Animator>().SetBool("isRun" , true);
        player.GetComponent<Character>().Health += healthSaver;
        player.GetComponent<Character>().HealthBar.fillAmount += (float)healthSaver/100;
        PlayerPrefs.SetInt("Health", healthSaver);
        Boss.GetComponent<PatrollBoss>().Health = 0;
        Boss.GetComponent<PatrollBoss>().Health += BossHPSaver;
        Boss.GetComponent<PatrollBoss>().HPBar.fillAmount = 1f;
        PlayerPrefs.SetInt("BossHP", BossHPSaver);
        BoolPlayerPrefs.SetBool("is2jump", doublejumpChecker);
        BoolPlayerPrefs.SetBool("canFly", parashuteChecker);
        BoolPlayerPrefs.SetBool("canThrow", canthrowChecker);
        if (canthrowChecker == false)
        {
            if (GameObject.Find("Kunai(Clone)"))
            {
                Debug.Log("Object already exist");
            }
            else
            {
                GameObject go = Instantiate(throwObject, canthrowSpawn.transform.position, Quaternion.identity);
            }
            
            trapObject.GetComponent<Animator>().SetBool("isTrap", false);
        }
       

    }
	public void UpdateCheckpoint (GameObject curCheck){
		curCheck.GetComponent<checkpoint> ().status = checkpoint.state.Active;
        healthSaver = PlayerPrefs.GetInt("Health");
        BossHPSaver = PlayerPrefs.GetInt("BossHP");
        doublejumpChecker = BoolPlayerPrefs.GetBool("is2jump");
        parashuteChecker = BoolPlayerPrefs.GetBool("canFly");
        canthrowChecker = BoolPlayerPrefs.GetBool("canThrow");

        Debug.Log(healthSaver);
        yourButton.interactable = true;

        foreach (GameObject cp in CHECPOINTS) {
			if (cp.GetComponent<checkpoint> ().status != checkpoint.state.Active) {
				cp.GetComponent<checkpoint> ().status = checkpoint.state.Inactive;
			}	
		}
	}
}
