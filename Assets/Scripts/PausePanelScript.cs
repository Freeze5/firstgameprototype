﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausePanelScript : MonoBehaviour {
    public GameObject pausePanel;
    public GameObject level;
   
    public bool panelON;
    public float maxTimer = 1f;
    public float timer;
    
	// Use this for initialization
	void Start () {
        panelON = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (panelON)
            timer += Time.deltaTime;
        else
            timer = 0;

        FreezeTime();
    }
    public void Resume()
    {
        panelON = false;
        pausePanel.GetComponent<Animator>().SetBool("PanelON", false);
        Time.timeScale = 1;
    }
    public void MainMenuButton()
    {
        panelON = false;
        pausePanel.GetComponent<Animator>().SetBool("PanelON", false);
        level.SetActive(false);
      
        SceneManager.LoadScene("Main Menu");
    }
    public void ShopButton()
    {
        panelON = false;
        pausePanel.GetComponent<Animator>().SetBool("PanelON", false);
        level.SetActive(false);
        
        SceneManager.LoadScene("shop");
    }
    public void Pause()
    {
        panelON = true;
        pausePanel.GetComponent<Animator>().SetBool("PanelON", true);
        
        
        
    }
    public void FreezeTime()
    {
        if (timer > maxTimer)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;
    }

}
