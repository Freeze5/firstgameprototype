﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class femaleZOMBIE : MonoBehaviour {
	public Vector2 StartPos;
	public Vector2 EndPos;
	public float speed;
	public float timer;
	public float Health;
	public Image HealthBar;
	public bool isHorizontal;
	public bool isRight;
	public bool Stay;
	public bool  isDead;
	public bool isAttack;
	public Vector2 Left;
	public Vector2 Right;



	// Use this for initialization
	void Start () {
		StartPos = transform.position;
		
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Mathf.Round (transform.position.x) == (int)StartPos.x) {
			isRight = true;
		}
		if (Mathf.Round (transform.position.x) == (int)EndPos.x) {
			isRight = false;
		}
		MoveHorizontal ();
	
	}
	void MoveHorizontal (){
		if (!isDead) {
			if (!Stay) {
				if (isRight) {
					ChangePosCol (Right);
					transform.position = Vector2.MoveTowards (transform.position, EndPos, speed);
					GetComponent< SpriteRenderer> ().flipX = false;

				}
			   else	if (!isRight) {
					ChangePosCol (Left);
					transform.position = Vector2.MoveTowards (transform.position, StartPos, speed);
					GetComponent<SpriteRenderer> ().flipX = true;
				
				}
			}
		}
	}
	void ChangePosCol ( Vector2 Pos ){
		BoxCollider2D[] mas = GetComponents<BoxCollider2D> ();
		for (int i = 0; i < mas.Length; i++) {
			if (mas [i].isTrigger = true) {
				mas [i].offset = Pos;
			}
		}
	}
}
