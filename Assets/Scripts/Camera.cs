﻿using UnityEngine;
using System.Collections;

public class Camera : MonoBehaviour {
	public Transform target;
	public float speed;
	public GameObject Character;

	// Use this for initialization
	void Start () {
		//Character = GameObject.Find ("Character_1");
	
	}

	// Update is called once per frame
	void Update () {
	
		//transform.position = new Vector3 (target.position.x, -3.21f, -10);
		if (Character.GetComponent<SpriteRenderer> ().flipX == false) {
			
			transform.position =  Vector3.Lerp(transform.position, new Vector3 (target.position.x + 6f, target.position.y, -10), speed);
		} else {
			transform.position = Vector3.Lerp (transform.position, new Vector3 (target.position.x - 6f,target.position.y, -10), speed);
		}


	}
}
