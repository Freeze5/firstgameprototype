﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Character : MonoBehaviour {
	// movementVAriables
	public float speed;
	float move;
	public bool isRun;
	public bool isAttack;
    float horizInput = 0f;// UI Buttons variable horizontal
    float verticalInput = 0f;//UI buttons variable vertical
	//bonusSpeed
	public float InitalSpeed;
	public float runFaster;
	// jumpVariables
	public bool isJump;
	public bool grounded;
	public float jumpPower;
	public float jumpTimer;
	public bool canJump;
	public float maxTime = 0.1f;
	// DoubleJumpVariables
	public bool isDoubleJump;
	public int Totaljump=1;
	public int currentJump;
    public GameObject bonusBridge;
	// healthVariables
	public int Health ;
	public Image HealthBar;
	public bool isDead;
	// coinsVariables
	public int CoinCount;
	public Text Coins;
	// ladderVariables
	public float ladderSpeed;
	public bool isClimb;
	// Parachute
	public bool canFly;//enable parachute button appear
	public bool isGlide;
	// PanelsVariables
	public GameObject deaDPanel;
    public GameObject parachute;
    public Button pauseBtn;
    
    
    
    


    void Awake()
    {
        deaDPanel = GameObject.Find("deadPanel");
        deaDPanel.SetActive(false);
    }
  
	// Use this for initialization
	void Start () {
		CoinCount = 0;
       
        parachute = GameObject.Find("Parachute");
        parachute.SetActive(false);
        bonusBridge = GameObject.Find("BonusBridge");
        //PlayerPrefs.SetInt("Health", Health);
        Health = PlayerPrefs.GetInt("Health");
        CoinCount = PlayerPrefs.GetInt("MyCoins");
        Coins.text = "Coins: " + PlayerPrefs.GetInt("MyCoins").ToString();
        HealthBar.fillAmount = 1f;//шкала здоровья 100
        HealthBar.fillAmount -=(float)(100 - PlayerPrefs.GetInt("Health"))/100; // отнять от шкалы разницу от изменения здоровья
        isDoubleJump =  BoolPlayerPrefs.GetBool("is2jump");
        canFly = BoolPlayerPrefs.GetBool("canFly");

        if (!isDead )
        {
            LoadPlayerPosition();
        }
    }
	void FixedUpdate()
    {
        //MoveCode using UIButtons
        MoveX(horizInput);
        if (isClimb)
        {
            isGlide = false;
            GetComponent<Animator>().SetBool("isGlide", false);
            GetComponent<Rigidbody2D>().drag = 0;
        }
           




    }
	// Update is called once per frame
	void Update () {
		if (!isDead) {
            // MOvementCODE without UI Buttons
            //move = Input.GetAxis ("Horizontal");
            //GetComponent<Rigidbody2D> ().velocity = new Vector2 (move * speed, GetComponent<Rigidbody2D> ().velocity.y);
            
			if ( horizInput <0) {
				GetComponent<Animator> ().SetBool ("isRun", true);
                transform.localScale = new Vector3(-0.6f, 0.6f, 1);
                
               // GetComponent<SpriteRenderer>().flipX = true;
            }
			if (horizInput > 0) {
				GetComponent<Animator> ().SetBool ("isRun", true);
                transform.localScale = new Vector3(0.6f, 0.6f, 1);
               
                // GetComponent<SpriteRenderer>().flipX = false;
            }
			if (horizInput == 0) {
				GetComponent<Animator> ().SetBool ("isRun", false);
			}
			//bonusSpeed
			speed = Input.GetKey (KeyCode.LeftShift) ? InitalSpeed * runFaster : InitalSpeed;
			// JumpCODE_isDoubleJump using keyboard
			if (isDoubleJump) {
				if (Input.GetKeyDown (KeyCode.Space) && !grounded && currentJump > 0) {
					GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x, 12);
					GetComponent<Animator> ().SetBool ("isJump", true);
					jumpTimer = 0;
					currentJump--;
				} else if (Input.GetKeyDown (KeyCode.Space) && grounded == true) {
					GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, jumpPower * 2.5f));
					GetComponent<Animator> ().SetBool ("isJump", true);
					jumpTimer = 0;
					canJump = true;
					grounded = false;
				} else if (Input.GetKey (KeyCode.Space) && canJump && jumpTimer < maxTime) {
					GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, jumpPower));
					jumpTimer += Time.deltaTime;
					//canJump = false; 
				} else {
					canJump = false;
				}
				if (grounded) {
					currentJump = Totaljump;
				}
			}
			// JumpCode_!isDoublejump
			if (!isDoubleJump) {
				if (Input.GetKeyDown (KeyCode.Space) && grounded == true) {
                    HeroSoundManager.PlaySound("Jump");
					GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, jumpPower * 2.5f));
					GetComponent<Animator> ().SetBool ("isJump", true);
					jumpTimer = 0;
					canJump = true;
					grounded = false;
				} else if (Input.GetKey (KeyCode.Space) && canJump && jumpTimer < maxTime) {
					GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, jumpPower));
					jumpTimer += Time.deltaTime;
					//canJump = false; 
				} else {
					canJump = false;
				
				}
			}
            //ParashuteCode using UI
            ParachuteCode();
            if (grounded == true)
                parachute.GetComponent<Image>().color = Color.red;
            else if (!grounded && currentJump ==0)
                parachute.GetComponent<Image>().color = Color.green;
			// AttackCODE
			if (Input.GetKeyDown (KeyCode.E)) {
				isAttack = true;
				GetComponent<Animator> ().SetBool ("isAttack", true);
			}
			if (Input.GetKeyUp (KeyCode.E)) {
				isAttack = false;
				GetComponent<Animator> ().SetBool ("isAttack", false);
			}
		}
		if (Health <= 0) {
			Health = 0;
            deaDPanel.SetActive(true);
            isDead = true;
            pauseBtn.GetComponent<Button>().interactable = false;
            GetComponent<Animator>().SetBool("isDead", true);
        }
        PlayerPrefs.SetFloat("HeroX", transform.localPosition.x);
        PlayerPrefs.SetFloat("HeroY", transform.localPosition.y);
        
        if (isClimb)
        {
            GetComponent<Animator>().SetBool("isJump", false);
            GetComponent<Animator>().SetBool("isRun", false);
            GetComponent<Animator>().SetBool("isGlide", false);
        }
        




    }
	void OnCollisionEnter2D (Collision2D col){
		if (col.gameObject.tag == "ground") {
			grounded = true;
			GetComponent<Animator> ().SetBool ("isJump", false);
		}
		if (col.gameObject.tag == "acid") {
			isDead = true;
			GetComponent<Animator> ().SetBool ("isDead", true);
			TakeDamage (100);
		}
		if (col.gameObject.tag == "spike") {
			isDead = true;
			GetComponent<Animator> ().SetBool ("isDead", true);
			TakeDamage (100);
		}
		if (col.gameObject.name == "transfer_1") {
			transform.parent = col.transform;
            pauseBtn.interactable = false;
        }
		if (col.gameObject.name == "lift") {
			col.gameObject.GetComponent<Lift1> ().enabled = true;
			transform.parent = col.transform;
            pauseBtn.interactable = false;
        }
		if (col.gameObject.name == "lift2") {
			col.gameObject.GetComponent<Lift_2> ().enabled = true;
			transform.parent = col.transform;
            pauseBtn.interactable = false;
        }
		if (col.gameObject.name == "lift3") {
			col.gameObject.GetComponent<lift3> ().enabled = true;
			transform.parent = col.transform;
            pauseBtn.interactable = false;
        }
		if (col.gameObject.tag == "saw") {
            Health -= Health ;
            HealthBar.fillAmount = 0f;
            isDead = true;
			GetComponent<Animator> ().SetBool ("isDead", true);
			
		}
		if (col.gameObject.tag == "teleport") {
			transform.position = new Vector2 (75f, 56f);
		}
		if (col.gameObject.tag == "spikeMonster") {
			TakeDamage (20);
			GetComponent<Rigidbody2D>().velocity =  Vector2.zero;
			//GetComponent<Rigidbody2D> ().AddForce (transform.up * 7f, ForceMode2D.Impulse);
			GetComponent<Rigidbody2D>().AddForce ( new Vector2 (250 , 500));
		}
	}
	void OnTriggerEnter2D ( Collider2D other ){
		if (other.gameObject.tag == "coin") {
            HeroSoundManager.PlaySound("Points");
            CollectCoins(other.gameObject);
			Coins.text = "Coins: " + CoinCount.ToString().ToUpper();
		}
		if (other.gameObject.name == "JumpBonus") {
            HeroSoundManager.PlaySound("Bonus");
			Destroy (other.gameObject);
            Destroy(bonusBridge);
			isDoubleJump = true;
            BoolPlayerPrefs.SetBool("is2jump", true);
		}
		if (other.gameObject.name == "Glide") {
            HeroSoundManager.PlaySound("Bonus");
			Destroy (other.gameObject);
			canFly = true;
            BoolPlayerPrefs.SetBool("canFly", true);
		}
		if (other.gameObject.tag == "bullet") {
			TakeDamage (25);
			GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
			GetComponent<Rigidbody2D> ().AddForce (new Vector2 (250, 500));

		}
	}
	void OnCollisionExit2D (Collision2D col){
		transform.parent = null;
        pauseBtn.interactable = true;
        if (col.gameObject.name == "lift") {
			//col.gameObject.GetComponent<Lift1> ().enabled = false; //отключить лифт если герой не на нем
		}
		if (col.gameObject.name == "lift2") {
			//col.gameObject.GetComponent<Lift_2> ().enabled = false;
		}
	}
	void OnTriggerStay2D ( Collider2D other){
		// ClimbLadder
		if (other.gameObject.tag == "ladder") {
            MoveY(verticalInput);
            if (Input.GetKey(KeyCode.W) || verticalInput == 1f)
            {
                this.transform.Translate(new Vector3(0, 1, 0) * Time.deltaTime * ladderSpeed);
                GetComponent<Animator>().SetBool("isClimb", true);
                
                isClimb = true;
            }
            else if (Input.GetKey(KeyCode.S) || verticalInput == -1f)
            {
                this.transform.Translate(new Vector3(0, -1, 0) * Time.deltaTime * ladderSpeed);
                GetComponent<Animator>().SetBool("isClimb", true);
               
                isClimb = true;
            }
            else if (verticalInput == 0f ) 
            {
                this.transform.Translate(new Vector3(0, 0, 0) * Time.deltaTime);
                GetComponent<Animator>().SetBool("isClimb", true);

                isClimb = true;
            }
		}
	}
	void OnTriggerExit2D ( Collider2D other){
		GetComponent<Animator> ().SetBool ("isClimb", false);
        isClimb = false;
	}
	public void TakeDamage ( int CountDamage){
		GameOver (Health);
		Health -= CountDamage;
		HealthBar.fillAmount -= (float)CountDamage / 100;
        PlayerPrefs.SetInt("Health", Health);
    }   

    void CollectCoins ( GameObject coins)
    {
        Destroy(coins);
        CoinCount++;
        int MyC = PlayerPrefs.GetInt("MyCoins");
        MyC = MyC + 1;
        PlayerPrefs.SetInt("MyCoins", MyC);

    }
	public void GameOver (int Over){
		if (Over == 0) {
			isDead = true;
			GetComponent<Animator> ().SetBool ("isDead", true);
            
            deaDPanel.SetActive (true);
		}
	}
    public void REturnHEalth(int returnHelth)
    {
       // GameOver(Health);
        Health += returnHelth;
        HealthBar.fillAmount += (float)returnHelth;
    }

    
    public void LoadPlayerPosition()
    {
        transform.localPosition = new Vector3(PlayerPrefs.GetFloat("HeroX"), PlayerPrefs.GetFloat("HeroY"), transform.position.z);
    }

    //Move horizontal using UI Buttons
    void MoveX ( float horizontalInput)
    {
        if (!isDead)
        {
            Vector2 moveVelocity = GetComponent<Rigidbody2D>().velocity;
            moveVelocity.x = horizontalInput * speed;
            GetComponent<Rigidbody2D>().velocity = moveVelocity;
        }

    }
    //method for checking a variable for event trigger at UI button
    public void StartMoving( float horizontalInput)
    {
        horizInput = horizontalInput;
    }
    //move vertical using UI buttons
    void MoveY( float verticalInput)
    {
        if (!isDead)
        {
            Vector2 moveVeloc = GetComponent<Rigidbody2D>().velocity;
            moveVeloc.y = verticalInput * speed;
            GetComponent<Rigidbody2D>().velocity = moveVeloc;
        }
    }
    //method for checking a variable for event trigger at UI button
    public void StartVertical(float verticalInp)
    {
        verticalInput = verticalInp;
    }
    //ParachuteCode using UI Buttons
    public void ParachuteCode()
    {
        if (canFly)
        {
            parachute.SetActive(true);
            if (isDoubleJump && !grounded && currentJump ==0)
            {
                isGlide = true;
                GetComponent<Animator>().SetBool("isGlide", true);
                GetComponent<Rigidbody2D>().drag = 12;
            }
            else if (!isDoubleJump && !grounded || grounded )
            {
                isGlide = false;
                GetComponent<Animator>().SetBool("isGlide", false);
                GetComponent<Rigidbody2D>().drag = 0;
            }
            
        }
    }
   
    //JumpCode using UIButton
    public void JumpButton()
    {

        //!isDoubleJump
        if (!isDoubleJump)
        {
            if (grounded == true)
            {
                HeroSoundManager.PlaySound("Jump");
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpPower * 7.5f));
                GetComponent<Animator>().SetBool("isJump", true);
                jumpTimer = 0;
                canJump = true;
                grounded = false;
            }
            else if (canJump && jumpTimer < maxTime)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpPower));
                jumpTimer += Time.deltaTime;
            }
            else
            {
                canJump = false;

            }
        }
            //isDoubleJump
            if (isDoubleJump)
            {
                if ( !grounded && currentJump > 0)
                {
                    GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, 12);
                    GetComponent<Animator>().SetBool("isJump", true);
                    jumpTimer = 0;
                    currentJump--;
                }
                else if ( grounded == true)
                {
                HeroSoundManager.PlaySound("Jump");
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpPower * 7.5f));
                    GetComponent<Animator>().SetBool("isJump", true);
                    jumpTimer = 0;
                    canJump = true;
                    grounded = false;
                }
                else if ( canJump && jumpTimer < maxTime)
                {
                    GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpPower));
                    jumpTimer += Time.deltaTime;
                }
                else
                {
                    canJump = false;
                }
                if (grounded)
                {
                    currentJump = Totaljump;
                }
            }
        }
    }

